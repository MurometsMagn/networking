package org.example.task_1_TCP_Sockets;

/*
Реализовать следующие задачи последовательно друг за другом:
1. Написать серверное приложение, которое ожидает подключения клиента,
после подключения ожидает получить от него строку текста произвольной длины,
после чего переворачивает эту строку ("hello" становится "olleh"), отправляет ее клиенту,
закрывает соединение и завершается.
Написать клиентское приложение, которое запрашивает у пользователя строку произвольной длины,
 подключается к созданному ранее серверу, отправляет ему эту строку,
 дожидается ответа, выводит его на экран, закрывает соединение и завершается.
2. Дополнить серверное приложение из предыдущей задачи тем, что после того,
как сервер обработал запрос клиента, он возвращается к ожиданию следующего клиента.
Таким образом, он самостоятельно никогда не завершается.
3. Дополнить серверное приложение из предыдущих задач тем,
что для обработки запросов клиентов используется пул потоков:
основной поток ожидает подключения клиента,
после подключения отправляет задачу в пул вместе с полученным клиентским сокетом,
после чего возвращается к ожиданию нового клиента.
Такой подход позволяет обрабатывать несколько клиентов одновременно.
 */

public class Main {
}
