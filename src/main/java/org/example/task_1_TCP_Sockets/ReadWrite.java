package org.example.task_1_TCP_Sockets;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class ReadWrite {

    public static void writing(BufferedWriter out, String line) throws IOException {
        String lineLength = line.length() + "";
        System.out.println("lineLength = " + lineLength);
        out.write(lineLength);
        out.write("\n");
        out.write(line);
        out.write("\n"); //каждая строка должна заканчиваться переводом строки
        out.flush();
        System.out.println("line = " + line);
    }

    @NotNull
    public static String reading(BufferedReader in) throws IOException {
        String otlLength = in.readLine();
        int lineLength = Integer.parseInt(otlLength);
        String line;
        System.out.println("otlLength = " + otlLength);
        StringBuilder stringBuilder = new StringBuilder();
        while (stringBuilder.length() < lineLength) {
            String readLine = in.readLine();
            stringBuilder.append(readLine);
            System.out.println("readLine = " + readLine);
        }
        line = stringBuilder.toString();
        System.out.println("line = " + line);
        return line;
    }
}
