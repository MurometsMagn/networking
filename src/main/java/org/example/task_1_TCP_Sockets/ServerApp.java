package org.example.task_1_TCP_Sockets;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerApp {

    public static void main(String[] args) throws IOException {
        new ServerApp();
    }

    public ServerApp() throws IOException {
        //System.out.println("ServerApp Started");
        InetAddress address = InetAddress.getByName("127.0.0.1");
        try (ServerSocket server = new ServerSocket(1337, 50, address);
             Socket client = server.accept();
             BufferedReader in = new BufferedReader(
                     new InputStreamReader(client.getInputStream())
             );
             BufferedWriter out = new BufferedWriter(
                     new OutputStreamWriter(client.getOutputStream())
             )
        ) {
            String line = ReadWrite.reading(in);
            line = invert(line);
            ReadWrite.writing(out,line);
        } //блок finally не нужен т.к. server и client находятся в try with resources
    }

    private String invert(String txt) {
        return new StringBuilder(txt).reverse().toString();
    }

    //invert charAt and substring
    private String invert2(String txt) {
        StringBuilder stringBuilder = new StringBuilder();
        int txtLength = txt.length();
        for (int i = 0; i < txtLength; i++) { //for (int i = (txt.length() - 1; i >= 0; i--) - можно так?
            stringBuilder.append(txt.charAt(txtLength - i - 1));
        }
        return stringBuilder.toString();
    }
}
