package org.example.task_1_TCP_Sockets;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientApp {
    private final Socket socket = new Socket("127.0.0.1", 1337);
    private final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        new ClientApp();
    }

    public ClientApp() throws IOException {
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream())
        );
             BufferedWriter out = new BufferedWriter(
                     new OutputStreamWriter(socket.getOutputStream())
             )
        ) {
            String line = this.scanner.nextLine();
            ReadWrite.writing(out, line);
            System.out.println(ReadWrite.reading(in));
        } finally {
            socket.close();
        }
    }


}
