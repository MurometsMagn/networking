package org.example.task_1_TCP_Sockets.fromDmitry;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private final ServerSocket serverSocket;
    private final ExecutorService pool;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        pool = Executors.newFixedThreadPool(4);
    }

    public void run() throws IOException {
        System.err.println("Server started");
        while (true) {
            Socket socket = serverSocket.accept();
            pool.execute(new ConnectionHandler(socket));
        }
    }

    public static void main(String[] args) {
        try {
            new Server(1337).run();
        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }
}
