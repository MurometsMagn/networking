package org.example.task_1_TCP_Sockets.fromDmitry;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter text to revert: ");
        String data = scanner.nextLine();

        try {
            Socket socket = new Socket("127.0.0.1", 1337);
            try (BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());
                 BufferedInputStream in = new BufferedInputStream(socket.getInputStream())
            ) {
                Sockets.sendString(out, data);
                data = Sockets.receiveString(in);
                System.out.println(data);
            }
            socket.close();
        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }
}