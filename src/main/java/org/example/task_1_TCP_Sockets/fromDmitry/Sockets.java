package org.example.task_1_TCP_Sockets.fromDmitry;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class Sockets {
    public static String receiveString(BufferedInputStream stream) throws IOException {
        byte[] lengthBuffer = readAll(stream, 2);
        int length = ByteBuffer.wrap(lengthBuffer).getShort();
        byte[] dataBuffer = readAll(stream, length);
        return new String(dataBuffer, StandardCharsets.UTF_8);
    }

    private static byte[] readAll(BufferedInputStream stream, int size) throws IOException {
        byte[] buffer = new byte[size];
        int totalBytesRead = 0;

        while (totalBytesRead < size) {
            int bytesRead = stream.read(buffer, totalBytesRead, size - totalBytesRead);
            totalBytesRead += bytesRead;
        }

        return buffer;
    }

    public static void sendString(BufferedOutputStream stream, String data) throws IOException {
        byte[] encodedData = data.getBytes();
        short bytesCount = (short)encodedData.length;
        ByteBuffer buffer = ByteBuffer.allocate(bytesCount + 2);
        buffer.putShort(bytesCount);
        buffer.put(encodedData);
        byte[] dataToSend = buffer.array();

        stream.write(dataToSend);
        stream.flush();
    }
}