package org.example.task_1_TCP_Sockets.fromDmitry;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ConnectionHandler implements Runnable {
    private Socket socket;

    public ConnectionHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());
             BufferedInputStream in = new BufferedInputStream(socket.getInputStream())
        ) {
            String data = Sockets.receiveString(in);
            data = new StringBuffer(data).reverse().toString(); //почему СтрингБуфер, а не СтрингБилдер?
            Sockets.sendString(out, data);
            socket.close();
        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }
}