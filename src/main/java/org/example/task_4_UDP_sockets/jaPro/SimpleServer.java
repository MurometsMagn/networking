package org.example.task_4_UDP_sockets.jaPro;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

//Минимальный TCP/IP-сервер
public class SimpleServer {
    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        //регестрируем наш сервис на порт 5432
        try {
            serverSocket = new ServerSocket(5432);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //бесконечный цикл слушателя подключений
        while (true) {
            try {
                //ждем подключения
                Socket socket = serverSocket.accept();
                //получаем выходной поток
                OutputStream outputStream = socket.getOutputStream();
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
                //отправляем сообщение
                dataOutputStream.writeUTF("Hello Net World");
                //закрываем подключение, но не серверный сокет
                dataOutputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
