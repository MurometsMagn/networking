package org.example.task_4_UDP_sockets.jaPro;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.Socket;

//Минимальный TCP/IP-клиент
public class SimpleClient {
    public static void main(String[] args) {
        try {
            //подключаемся к серверу - порт 5432
            //имя машины - localhost :-)
            Socket socket = new Socket("127.0.0.1", 5432);
            //получаем входной поток
            InputStream inputStream = socket.getInputStream();
            //декорируем его с DataInputStream
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            //читаем из потока и выводим на экран
            System.out.println(dataInputStream.readUTF());
            //по окончанию - закрываем поток и подключение
            dataInputStream.close();
            socket.close();
        } catch (ConnectException connectException) {
            System.err.println("Could not connect to the server.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
