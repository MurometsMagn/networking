package org.example.task_4_UDP_sockets.jaPro;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

//узнать полную инфу о доменном имени с помощью сокета
//не идет((
public class WhoIs {
    public static void main(String[] args) throws Exception {
        try {
            int c;
            //Создает сокетное соединение с internic.net, порт 43.
            //Socket s = new Socket("internic.net", 43);
            Socket s = new Socket("google.com", 80);
            //Получает входной и выходной потоки.
            InputStream in = s.getInputStream();
            OutputStream out = s.getOutputStream();
            //конструирует строку запроса.
            //String str = (args.length == 0 ? "GET /" : args[0]) + "\n";
            String str = (args.length == 0 ? "HEAD /" : args[0]) + "\n";
            //преобразует в байты
            byte[] buf = str.getBytes();
            //посылает запрос.
            out.write(buf);
            //читает и отображает ответ.
            while ((c = in.read()) != -1) {
                System.out.print((char) c);
            }
            s.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
