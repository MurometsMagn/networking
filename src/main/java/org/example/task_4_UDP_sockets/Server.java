package org.example.task_4_UDP_sockets;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class Server {
    private DatagramChannel channel;
    private SocketAddress addr = new InetSocketAddress("127.0.0.1", 1337);

    public Server() throws IOException {
        while (true) {
            run();
        }
    }

    private void run() throws IOException {
        int lastSeq = 0; // перенести во входящие параметры класса
        createServerChannel();
        receiveData(lastSeq);
    }

    private void createServerChannel() throws IOException {
        channel = DatagramChannel.open();
        channel.socket().bind(addr);
    }

    public void sendData(int seq, int value) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putInt(seq);
        buffer.putInt(value);
        buffer.flip();

        channel.send(buffer, addr);
    }

    public void receiveData(int lastSeq) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        SocketAddress addr = channel.receive(buffer);

        buffer.flip();
        int seq = buffer.getInt();
        int value = buffer.getInt();

        if (seq > lastSeq) {
            System.out.println("Received " + value + " from " + addr);
            lastSeq = seq;
        }
    }
}
