package org.example;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

/**
 * If some method is often used in dif projects.
 * Smolentsev Il'ya
 */

public class MyOwnLib {
    private static final Scanner scanner = new Scanner(System.in);
    private static final Random random = new Random();

    public static int keyboardInputInt(int minValue, int maxValue) {
        int inputData;
        while (true) {
            try {
                inputData = scanner.nextInt();
                if (inputData >= minValue && inputData <= maxValue)
                    break;
                else
                    System.out.println("некоректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        }
        return inputData;
    }

    public static String keyboardInputString(String requestToUser) {
        String inputData; //responseFromUser
        do {
            System.out.println(requestToUser);
            inputData = scanner.nextLine();
        } while (inputData.trim().isEmpty());
        return inputData;
    }

    public static int randomInt(int min, int max) { //включительно
        return (int) (Math.random() * (max - min + 1) + min);
        //для вещественных чисел убрать каст к инту
        //Math.random() - от ноля включительно до 1 не включительно
    }

    public static int randomInt2(int min, int max) { //включительно
        return random.nextInt(max - min + 1) + min;
        //Random от 0 включительно до bound не включительно
    }

    //вместо Thread.sleep(1000)
    public static void timing(int delay) {
        //int delay = 1000; // number of milliseconds to sleep

        long start = System.currentTimeMillis();
        while(start >= System.currentTimeMillis() - delay); // do nothing

        //System.out.println("Time Slept: " + Long.toString(System.currentTimeMillis() - start));
    }

    public static void timing(){
        timing(1000);
    }
}
