package org.example.task_2_Polling;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class Client {
    SocketChannel client = SocketChannel.open();

    public Client() throws IOException {
        client.connect(new InetSocketAddress("127.0.0.1", 1337));
    }
}
