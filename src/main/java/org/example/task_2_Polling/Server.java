package org.example.task_2_Polling;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {
    ServerSocketChannel server = ServerSocketChannel.open();

    public Server() throws IOException {
        server.socket().bind(new InetSocketAddress(1337));
        server.configureBlocking(false);

        Selector selector = Selector.open();
        server.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            selector.select();

            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> it = keys.iterator();
            while (it.hasNext()) {
                SelectionKey key = it.next();

                if (key.isAcceptable()) {
                    SocketChannel client = server.accept();
                    client.configureBlocking(false);
                    client.register(selector, SelectionKey.OP_READ, new Object());
                } else if (key.isReadable()) {
                    ByteBuffer buffer = ByteBuffer.allocate(1024);

                    SocketChannel client = (SocketChannel) key.channel();
                    int byteRead = client.read(buffer);
                    if (byteRead < 0) {
                        client.close();
                    } else {
                        buffer.flip();
                        client.write(buffer);
                    }
                }
                it.remove();
            }
        }
    }
}
