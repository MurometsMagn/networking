package org.example.task_3_TicTacToeNet.newPack.common;

public interface RequestMessageIDs {
    public static final byte NEW_GAME_MESSAGE = 1;
    public static final byte JOIN_GAME_MESSAGE = 2;
}
