package org.example.task_3_TicTacToeNet.newPack.common;

public interface ResponseMessageIDs {
    public static final byte INVALID_MESSAGE = -1;
    //0 - cообщение вообще не получили
    public static final byte OK_MESSAGE = 1;
    public static final byte WINNER_MESSAGE = 2;
    public static final byte LOSER_MESSAGE = 3;
    public static final byte DRAW_MESSAGE = 4;
    //loose
}
