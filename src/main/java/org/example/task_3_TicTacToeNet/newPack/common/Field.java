package org.example.task_3_TicTacToeNet.newPack.common;

//крестик всегда у первого игрока

public class Field {
    private char[][] field = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
    private int stepCounter = 0; //первый ход имеет номер 1

    public Field() {
    }

    public Field(char[][] field) {
        this.field = field;
    }

    public char getCell(int row, int column) {
        return field[row][column];
    }

    public void setCell(int row, int column, char contents) {
        field[row][column] = contents;
    }

    public boolean isCellFree(int row, int column) {
        return field[row][column] == ' ';
    }

    public boolean isWinner(int row, int column, char playerChar) {
        char[][] field = new char[3][3]; //локальный филд для отката изменений
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = this.field[i][j];
            }
        }
        field[row][column] = playerChar;
        return ((field[0][column] == playerChar && field[1][column] == playerChar && field[2][column] == playerChar)  //вертикаль
                || (field[row][0] == playerChar && field[row][1] == playerChar && field[row][2] == playerChar) //горизонталь
                || (((row == 0 && column == 0) || (row == 1 && column == 1) || (row == 2 && column == 2)) // лев диагональ
                && (field[0][0] == playerChar && field[1][1] == playerChar && field[2][2] == playerChar))
                || (((row == 2 && column == 0) || (row == 1 && column == 1) || (row == 0 && column == 2)) //пр диагональ
                && (field[2][0] == playerChar && field[1][1] == playerChar && field[0][2] == playerChar)));
    }

    public boolean isDraw() {
        return stepCounter >= 9;
    }

    public void increaseStepCounter() {
        stepCounter++;
    }

    public int getStepCounter() {
        return stepCounter;
    }

    public char getCurrentPlayerChar() {
        return stepCounter % 2 == 0 ? '0' : 'X';
    }

    public void makeStep(int row, int column) {
        char playerChar = getCurrentPlayerChar();
        setCell(row, column, playerChar);
    }
}
