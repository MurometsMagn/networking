package org.example.task_3_TicTacToeNet.newPack.server;

import org.example.task_3_TicTacToeNet.newPack.common.ResponseMessageIDs;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public abstract class AbstractState implements State {
    protected void handleOKMessage(SocketChannel clientChannel) throws IOException {
        sendResponse(clientChannel, ResponseMessageIDs.OK_MESSAGE);
        //clientChannel.write(ByteBuffer.allocate(1).put((byte) 1).flip());
    }

    //минус единичку резервируем как возвращамый код ошибки
    protected void handleInvalidMessage(SocketChannel clientChannel) throws IOException {
        sendResponse(clientChannel, ResponseMessageIDs.INVALID_MESSAGE);
        //sendResponse(clientChannel, (byte) -1);
    }

    protected void sendResponse(SocketChannel clientChannel, byte response) throws IOException {
        clientChannel.write(ByteBuffer.allocate(1).put(response).flip());
    }
}
