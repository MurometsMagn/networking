package org.example.task_3_TicTacToeNet.newPack.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Server {
    private Map<SocketChannel, Player> players = new HashMap<>();
    private Map<String, Game> games = new HashMap<>();
    private final ServerSocketChannel server = ServerSocketChannel.open();
    private Selector selector;

    public Server() throws IOException {
        server.socket().bind(new InetSocketAddress(1337));
        server.configureBlocking(false);
    }

    public void addPlayer(Player player, SocketChannel clientSocket) {
        players.put(clientSocket, player);
        Socket clSocket = clientSocket.socket(); //получение сокета из ченела
    }

    public Player getPlayerBySocket(SocketChannel clientSocket) {
        return players.get(clientSocket);
    }

    public void removePlayerBySocket(SocketChannel clientSocket) {
        Player player = players.get(clientSocket);
        if (player != null) {
            Game game = player.getGame();
            if (game != null) {
                games.values().remove(game);
            }
            players.remove(clientSocket);
        }
    }

    //запрос только от второго игрока
    public boolean isGameFree(String name) {
        Game game = games.get(name);
        if (game == null) return false;
        return !game.isReady();
    }

    //запрос только от первого игрока
    public boolean gameExists(String name) {
        return games.containsKey(name);
    }

    public void addGame(String name, Game game) {
        games.put(name, game);
    }

    public Game getGame(String name) {
        return games.get(name);
    }

    public void removeGame(String name) {
        Game game = games.get(name);
        if (game != null) {
            games.values().remove(game);
        }
    }

    public void run() throws IOException {
        //ожидаем событий в селекторе
        selector = Selector.open();
        server.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            selector.select();
            Set<SelectionKey> keys = selector.selectedKeys(); //событие
            Iterator<SelectionKey> it = keys.iterator();
            while (it.hasNext()) {
                try {
                    SelectionKey key = it.next();
                    if (key.isAcceptable()) { //пришел новый клиент
                        SocketChannel client = server.accept();
                        client.configureBlocking(false);
                        Player player = new Player(client, this);
                        players.put(client, player);
                        client.register(selector, SelectionKey.OP_READ, player); //
                    } else if (key.isReadable()) { //пришел старый (но неотключившийся) клиент
                        //SocketChannel client = (SocketChannel) key.channel();
                        Player player = (Player) key.attachment();
                        player.handleRead();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                it.remove();
            }
        }
    }

    public static void main(String[] args) {
        try {
            new Server().run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
