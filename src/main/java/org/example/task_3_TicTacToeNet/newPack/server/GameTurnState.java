package org.example.task_3_TicTacToeNet.newPack.server;

import org.example.task_3_TicTacToeNet.newPack.common.ResponseMessageIDs;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class GameTurnState extends AbstractState {
    private final Player player;
    private final ByteBuffer buffer = ByteBuffer.allocate(2); //два символа x, y

    public GameTurnState(Player player) {
        this.player = player;
    }

    @Override
    public void handleRead(SocketChannel clientChannel) throws IOException {
        clientChannel.read(buffer);
        if (buffer.hasRemaining()) return;
        buffer.flip();

        int row = buffer.get(); //гет возвращает один байт
        int column = buffer.get();
        Game game = player.getGame();
        Player opponent = game.getOpponent(player);
        if (!game.isValidPosition(row, column)) {
            handleInvalidMessage(clientChannel);
            return;
        }
        game.increaseStepCounter();

        char playerChar = game.getPlayerChar(player);
        game.setCell(row, column, playerChar);
        SocketChannel opponentChannel = opponent.getSocketChannel();
        sendTurn(opponentChannel, row, column);

        if (game.isWinner(row, column, playerChar)) { //победитель
            sendWinner(clientChannel);
            sendLoser(opponentChannel);
            clientChannel.close();
            opponentChannel.close();
            return;
        }
        if (game.isDraw()) { //ничья
            sendDraw(clientChannel);
            sendDraw(opponentChannel);
            clientChannel.close();
            opponentChannel.close();
            return;
        }
        //продолжение игры
        player.setState(new WaitTurnState());
        opponent.setState(new GameTurnState(opponent));
        handleOKMessage(clientChannel);
        handleOKMessage(opponentChannel);
    }

    public void sendWinner(SocketChannel clientChannel) throws IOException {
        sendResponse(clientChannel, ResponseMessageIDs.WINNER_MESSAGE);
    }

    private void sendLoser(SocketChannel clientChannel) throws IOException {
        sendResponse(clientChannel, ResponseMessageIDs.LOSER_MESSAGE);
    }

    private void sendDraw(SocketChannel clientChannel) throws IOException {
        //sendResponse(clientChannel, (byte) 4);
        sendResponse(clientChannel, ResponseMessageIDs.DRAW_MESSAGE);
    }

    private void sendTurn(SocketChannel clientChannel, int row, int column) throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        byteBuffer.putInt(row);
        byteBuffer.putInt(column);
        byteBuffer.flip();

        clientChannel.write(byteBuffer);
    }
}

//todo: homeTask 16.09
/**
 * 1.реализовать проверку ничьи
 * 2. в случае ничьи отправлять сообщение игрокам о ничьей
 * 3. во всех остальных случаях отправлять обоим игрокам "ОК" и меняем состояние
 */