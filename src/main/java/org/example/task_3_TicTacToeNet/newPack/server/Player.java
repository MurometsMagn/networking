package org.example.task_3_TicTacToeNet.newPack.server;

import java.io.IOException;
import java.nio.channels.SocketChannel;

public class Player {
    private Game game = null;
    private final SocketChannel socketChannel;
    private final Server server;
    private State state = new ClientConnectedState(this);

    public Player(SocketChannel socketChannel, Server server) throws IOException {
        this.socketChannel = socketChannel;
        this.server = server;
        server.addPlayer(this, socketChannel);
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void handleRead() throws IOException {
        state.handleRead(socketChannel);
    }

    public void setState(State state) {
        this.state = state;
    }

    public Server getServer() {
        return server;
    }

    public SocketChannel getSocketChannel() {
        return socketChannel;
    }
}
