package org.example.task_3_TicTacToeNet.newPack.server.compTurn;

import java.util.Random;

public class Point {
    private static final Random random = new Random();
    private final int row;
    private final int column;
    //private boolean shot = false;
    //private char content = ' ';

    public Point(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public boolean isValidCell(int row, int column) {
        return (row >= 0 && row < 3 && column >= 0 && column < 3);
    }

    public static Point createRandom() {
        return new Point(random.nextInt(3), // 0,1,2
                random.nextInt(3) );
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}
