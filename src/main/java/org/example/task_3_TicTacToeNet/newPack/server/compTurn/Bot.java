package org.example.task_3_TicTacToeNet.newPack.server.compTurn;

import org.example.task_3_TicTacToeNet.newPack.common.Field;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Bot {
    private static final Random random = new Random();
    private final Field field;
    private final char playerChar;
    //private final Player opponent;
    //private Game game1;

    public Bot(Field field, char playerChar) {
        this.field = field;
        this.playerChar = playerChar;
    }

    public void handleRead(SocketChannel clientChannel) throws IOException {
        Point point = compPlayerLogic();

        //после перебора всех вариантов ходов
        field.setCell(point.getRow(), point.getColumn(), playerChar);
        field.increaseStepCounter();
    }

    private Point compPlayerLogic() { //сделать глаголом..
        //1. перебрать клетки на возможность победы
        Point point;
        point = tryToWin(playerChar);
        if (point != null) return point;

        //2. иначе, перебрать клетки на возможность победы врага (и занять эту клетку)
        point = tryToWin(getOpponentChar());
        if (point != null) return point;

        //3. иначе, хардкор: ставим сперва в центр, потом в углы
        point = hardcorring();
        if (point != null) return point;

        //4. иначе рандом
        if (field.getStepCounter() < 6) {
            do {
                point = Point.createRandom();
            } while (isNotShot(point));
            return point;
        } else { //дальше не шибко нужная логика, но интересно было пописать))
            List<Point> pointList = new ArrayList<>();
            int max = 9 - field.getStepCounter();
            for (int row = 0; row < 3; row++) {
                for (int column = 0; column < 3; column++) {
                    point = new Point(row, column);
                    if (isNotShot(point)) pointList.add(point);
                }
            }
            point = pointList.get(random.nextInt(pointList.size())); //+1 вроде не нужен
            return point;
        }
    }

    private char getOpponentChar() {
        return playerChar == '0' ? 'X' : '0';
    }

    @Nullable
    private Point tryToWin(char plChar) {
        Point point;
        for (int row = 0; row < 3; row++) {
            for (int column = 0; column < 3; column++) {
                point = new Point(row, column);
                if (isNotShot(point) && field.isWinner(row, column, plChar)) {
                    return point;
                }
            }
        }
        return null;
    }

    @Nullable
    private Point hardcorring() {
        Point point;
        if (isNotShot(point = new Point(1, 1))) return point; //центр

        Point[] points = {new Point(0, 0), //четыре угла
                new Point(0, 2),
                new Point(2, 0),
                new Point(2, 2)};
        int cornerCounter = random.nextInt(4);
        for (int i = 0; i <= 4; i++) {
            if (cornerCounter == 4) cornerCounter = 0;
            point = points[cornerCounter];
            if (isNotShot(point)) return point;
            cornerCounter++;
        }

        return null;
    }

    public boolean isNotShot(Point point) {
        return (field.getCell(point.getRow(), point.getColumn()) == ' ');
    }
}

/*
как вариант: КомпТюрнСтэйт вызывает бота и передает в его конструктор гейм
 */
