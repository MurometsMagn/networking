package org.example.task_3_TicTacToeNet.newPack.server;

import java.io.IOException;
import java.nio.channels.SocketChannel;

public interface State {
    void handleRead(SocketChannel clientChannel) throws IOException;
}
