package org.example.task_3_TicTacToeNet.newPack.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class CreateGameState extends AbstractState {
    private final Player player;
    private final ByteBuffer buffer = ByteBuffer.allocate(128); //64 символа на кириллице

    public CreateGameState(Player player) {
        this.player = player;
    }

    @Override
    public void handleRead(SocketChannel clientChannel) throws IOException {
        clientChannel.read(buffer);
        if (buffer.hasRemaining()) { //если осталость свободное место в буфере
            return; //надо еще дочитывать
        }
        buffer.flip();
        String gameName = new String(buffer.array());

        Server server = player.getServer();
        if (!server.gameExists(gameName)) {
            Game game = new Game(gameName, player);
            server.addGame(gameName, game);
            player.setGame(game);
            player.setState(new WaitForSecondPlayerState());
            handleOKMessage(clientChannel);
        } else {
            handleInvalidMessage(clientChannel);
        }
    }
}

//libGDX
//LWJGL (openGL for java)
//на андроид опенДжиЭль предоставляется самим андроидом (без доп библиотек)
