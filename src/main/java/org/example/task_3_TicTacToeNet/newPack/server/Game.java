package org.example.task_3_TicTacToeNet.newPack.server;

import org.example.task_3_TicTacToeNet.newPack.common.Field;

import java.util.Random;

public class Game {
    private static final Random random = new Random();
    //private final char[][] field = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
    private final Field field = new Field();
    private final String name;
    private Player[] players = {null, null};


    public Game(String name, Player firstPlayer) {
        this.name = name;
        players[0] = firstPlayer;

    }

    public char getCell(int row, int column) {
        return field.getCell(row, column);
    }

    public void setCell(int row, int column, char contents) {
        field.setCell(row, column, contents);
    }

    public boolean isValidPosition(int row, int column) {
        return (row >= 0 && row < 3 && column >= 0 && column < 3
                && field.getCell(row, column) == ' ');
    }

    public void addPlayer(Player secondPlayer) {
        players[1] = secondPlayer;
    }

    public boolean isReady() {
        return players[0] != null && players[1] != null;
    }

    public int generateStartPlayer() {
        return random.nextInt(2);
    }

    public Player getPlayer(int numberPlayer) {
        return players[numberPlayer];
    }

    public Player getOpponent(Player player) {
        if (players[0] == player) return players[1];
        if (players[1] == player) return players[0];
        return null;
    }

    public char getPlayerChar(Player player) {
        if (players[0] == player) return 'X';
        if (players[1] == player) return '0';
        return '?';
    }

    public boolean isWinner(int row, int column, char playerChar) {
        return field.isWinner(row, column, playerChar);
    }

    public boolean isDraw() {
        return field.isDraw();
    }

    public void increaseStepCounter() {
        field.increaseStepCounter();
    }

    public char contentsChar(int row, int column) {
        return field.getCell(row, column);
    }

    public int getStepCounter() {
        return field.getStepCounter();
    }
}

//todo: homeTask 26.10.21
/*
 1. coздать сущность Филд одинаковую и для клиента и для сервера
 2. использовать класс Филд и на клиенте и на сервере вместо того, что там есть
 3. перенести метод isWinner в класс Филд
 4. применить в CompTurnState и вынести его в общий код
 */
