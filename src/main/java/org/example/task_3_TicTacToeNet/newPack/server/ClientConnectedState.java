package org.example.task_3_TicTacToeNet.newPack.server;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import static org.example.task_3_TicTacToeNet.newPack.common.RequestMessageIDs.JOIN_GAME_MESSAGE;
import static org.example.task_3_TicTacToeNet.newPack.common.RequestMessageIDs.NEW_GAME_MESSAGE;

public class ClientConnectedState extends AbstractState {
    private final Player player;

    public ClientConnectedState(Player player) throws IOException {
        this.player = player;
    }


    @Override
    public void handleRead(SocketChannel clientChannel) throws IOException {
        try {
            ByteBuffer buffer = ByteBuffer.allocate(1); //выделить память под буфер размером 1 байт
            clientChannel.read(buffer); //прочитать из сокета в буфер 1 байт (размер буфера)
            buffer.flip(); //переключиться в режим чтения
            byte message = buffer.get(); //получить байт из байт-буфера
            switch (message) {
                case NEW_GAME_MESSAGE -> {
                    player.setState(new CreateGameState(player));
                    handleOKMessage(clientChannel);
                }
                case JOIN_GAME_MESSAGE -> {
                    player.setState(new JoinGameState(player));
                    handleOKMessage(clientChannel);
                }
                default -> handleInvalidMessage(clientChannel);
            }
        } catch (BufferUnderflowException e) {
            clientChannel.close();
        }
    }
}
