package org.example.task_3_TicTacToeNet.newPack.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

//второй игрок присоединяется к существующей игре
public class JoinGameState extends AbstractState{
    private final Player player;
    private final ByteBuffer buffer = ByteBuffer.allocate(128);

    public JoinGameState(Player player) {
        this.player = player;
    }

    @Override
    public void handleRead(SocketChannel clientChannel) throws IOException {
        clientChannel.read(buffer);
        if (buffer.hasRemaining()) {
            return;
        }
        buffer.flip();
        String gameName = new String(buffer.array());

        Server server = player.getServer();
        if (server.isGameFree(gameName)) {
            Game game = server.getGame(gameName);
            game.addPlayer(player);
            player.setGame(game);
            int startPlayer = game.generateStartPlayer();
            game.getPlayer(startPlayer).setState(new GameTurnState(player));
            game.getPlayer(1 - startPlayer).setState(new WaitTurnState());
            handleOKMessage(clientChannel);
        } else {
            handleInvalidMessage(clientChannel);
        }
    }
}
