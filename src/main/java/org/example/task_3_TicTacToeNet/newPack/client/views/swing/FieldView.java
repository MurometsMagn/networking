package org.example.task_3_TicTacToeNet.newPack.client.views.swing;

import org.example.task_3_TicTacToeNet.newPack.common.Field;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public class FieldView extends JComponent {
    private static final int CELLS_IN_A_ROW = 3;
    private Field field;
    private int width;
    private int height;
    private int cellSize;
    //private BiFunction<Integer, Integer, Void> clickListener = null; //аналог Consumer с двумя параметрами: row, collumn
    private BiConsumer<Integer, Integer> clickListener = null; //аналог Consumer с двумя параметрами: row, collumn

    public FieldView(Field field) {
        this.field = field;
        enableEvents(AWTEvent.MOUSE_EVENT_MASK);

    }

    public void setClickListener(BiConsumer<Integer, Integer> clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        Dimension dimension = getSize();
        width = dimension.width;
        height = dimension.height;
        int min = Math.min(height, width);
        cellSize = min / CELLS_IN_A_ROW;
        width = height = cellSize * CELLS_IN_A_ROW;

        //отрисовка сетки
        for (int i = 0; i <= CELLS_IN_A_ROW; i++) {
            int x = cellSize * i;
            g.drawLine(x, 0, x, height);

            int y = cellSize * i;
            g.drawLine(0, y, width, y);
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int row = i * cellSize;
                int column = j * cellSize;
                if (field.getCell(i, j) == 'X') paintX(graphics2D, row, column, cellSize);
                else if (field.getCell(i, j) == '0') paint0(graphics2D, row, column, cellSize);
            }
        }
    }

    private void paint0(Graphics2D g, int row, int column, int cellSize) {
        Stroke oldStroke = g.getStroke();
        g.setStroke(new BasicStroke(3));
        g.drawOval(column + (cellSize / 6), row + (cellSize / 6),  cellSize * 2 / 3, cellSize * 2 / 3);

        g.setStroke(oldStroke);
    }

    private void paintX(Graphics2D g, int row, int column, int cellSize) {
        Stroke oldStroke = g.getStroke();
        g.setStroke(new BasicStroke(3));
        g.drawLine(row * cellSize, column * cellSize, (row + 1) * cellSize, (column + 1) * cellSize);
        g.drawLine((row + 1) * cellSize, column * cellSize, row * cellSize, (column + 1) * cellSize);

        g.setStroke(oldStroke);
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        super.processMouseEvent(e);
        if (SwingUtilities.isLeftMouseButton(e)) {
            System.out.println("mouse event");

            int pointX = e.getX();
            int pointY = e.getY();

            int row = pointY / cellSize;
            int column = pointX / cellSize;

            if (row >= 0 && row < 3 && column >= 0 && column < 3) {
//                if (field.isCellFree(row, column)) { //убрать в контроллер
//                    field.makeStep(row, column);
//                    System.out.println("repaint");
//                    //revalidate();
//                }
                if (clickListener != null) {
                    clickListener.accept(row, column);
                    repaint();
                }
            }
        }
        //todo: hometask 2.11.21
        // филд знает номер текущего хода, а значит - символ текущего игрока
        // написать метод в Филд, который делает ход за текущего игрока по указанным координатам
    }

    public static void main(String[] args) { //for tests
        JFrame frame = new JFrame("test");
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        Field field = new Field();
        field.setCell(0, 0, 'X');
        field.setCell(1, 0, 'x');
        field.setCell(1, 1, '0');
        FieldView fieldView = new FieldView(field);
        frame.add(fieldView);
        frame.setVisible(true);
    }
}

//todo: homeTask21.09.21
/*
 1. добавить отображение поля в методе ПэйнтКомпонент класса ФилдВью
 2. в методе processMouseEvent принять координаты нажатия мышью и
    выставить значение в экземпляре класса Филд.
 */

//todo: hometask 4.11.21
/*
 создать класс ГеймВью и поместить в него экземпляр ФилдВью
 добавить в ГеймВью jLabel с указанием текущего игрока x или 0
 */


