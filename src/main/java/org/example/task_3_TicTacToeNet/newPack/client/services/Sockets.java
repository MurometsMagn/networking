package org.example.task_3_TicTacToeNet.newPack.client.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class Sockets {
    public static String receiveString(BufferedInputStream stream) throws IOException {
        byte[] lengthBuffer = readAll(stream, 2);
        int length = ByteBuffer.wrap(lengthBuffer).getShort();
        byte[] dataBuffer = readAll(stream, length);
        return new String(dataBuffer, StandardCharsets.UTF_8);
    }

    public static byte[] readAll(BufferedInputStream stream, int size) throws IOException {
        byte[] buffer = new byte[size];
        int totalBytesRead = 0;

        while (totalBytesRead < size) {
            // stream.read гарантирует, что прочитает минимум один байт(!) и не больше заданного
            int bytesRead = stream.read(buffer, totalBytesRead, size - totalBytesRead);
            totalBytesRead += bytesRead;
        }

        return buffer;
    }

    public static void sendString(BufferedOutputStream stream, String data) throws IOException {
        byte[] encodedData = data.getBytes();
        short bytesCount = (short)encodedData.length;
        ByteBuffer buffer = ByteBuffer.allocate(bytesCount + 2);
        buffer.putShort(bytesCount);
        buffer.put(encodedData);
        byte[] dataToSend = buffer.array();

        stream.write(dataToSend);
        stream.flush();
    }

    public static byte receiveSingleByte(BufferedInputStream stream) throws IOException{
        byte[] message = new byte[1];
        int messageSize = stream.read(message);
        return message[0];
    }

    public static void sendSingleByte(BufferedOutputStream stream, byte data) throws IOException {
        byte[] encodedData = {data};
        stream.write(encodedData);
        stream.flush();
    }

    public static void sendAllBytes(BufferedOutputStream stream, byte[] data) throws IOException {
        short bytesCount = (short)data.length;
        ByteBuffer buffer = ByteBuffer.allocate(bytesCount + 2);
        buffer.putShort(bytesCount);
        buffer.put(data);
        byte[] dataToSend = buffer.array();

        stream.write(dataToSend);
        stream.flush();
    }
}

/* todo: homeTask 30.09.21
  1. написать метод отправки массива байт целиком
  2. отправить имя игры на сервер и получить ответ
 */