package org.example.task_3_TicTacToeNet.newPack.client.controllers;

import org.example.task_3_TicTacToeNet.newPack.client.services.NetworkService;
import org.example.task_3_TicTacToeNet.newPack.client.views.swing.GameView;
import org.example.task_3_TicTacToeNet.newPack.common.Field;
import org.example.task_3_TicTacToeNet.newPack.common.ResponseMessageIDs;

import java.io.IOException;

public class WaitState implements GameState{
    private final GameController controller;
    private final GameView gameView;
    private final Field field;
    private final NetworkService networkService;

    public WaitState(GameController controller, GameView gameView, Field field, NetworkService networkService) {
        this.controller = controller;
        this.gameView = gameView;
        this.field = field;
        this.networkService = networkService;
    }

    @Override
    public void run() {
        int row, column;
        try {
            int[] turn = networkService.getOpponentTurn();
            row = turn[0];
            column = turn[1];
        } catch (IOException e) {
            gameView.showMessage("Network error");
            return;
        }
        field.makeStep(row, column);

        byte turnResult;
        try {
            turnResult = networkService.receiveTurnResult();
        } catch (IOException e) {
            gameView.showMessage("Network error");
            return;
        }
        if (turnResult == ResponseMessageIDs.OK_MESSAGE) {
            field.increaseStepCounter();
            controller.setState(new PlayState(controller, gameView, field, networkService));
        } else {
            controller.setState(new GameOverState());
        }
    }
}

//todo: homeTask 11.11.21
/*
 1. В JoinGameState (в клиенте) дождаться ответа от сервера и переключиться на PlayState или WaitState
 2. В WaitForSecondPlayerState (в клиенте) дождаться ответа от сервера и переключиться на PlayState или WaitState
 3. В PlayState реализовать метод run();
 */