package org.example.task_3_TicTacToeNet.newPack.client.controllers;

import org.example.task_3_TicTacToeNet.newPack.client.services.NetworkService;
import org.example.task_3_TicTacToeNet.newPack.client.views.swing.GameView;
import org.example.task_3_TicTacToeNet.newPack.common.Field;
import org.example.task_3_TicTacToeNet.newPack.common.ResponseMessageIDs;

import java.io.IOException;

public class PlayState implements GameState {
    private final GameController controller;
    private final GameView gameView;
    private final Field field;
    private final NetworkService networkService;

    public PlayState(GameController controller, GameView gameView, Field field, NetworkService networkService) {
        this.controller = controller;
        this.gameView = gameView;
        this.field = field;
        this.networkService = networkService;

        gameView.setFieldViewClickListener(this::handleFieldClick);
    }

    private void handleFieldClick(int row, int column) {
        if (field.isCellFree(row, column)) {
            byte sendTurnResponse = 0;
            try {
                sendTurnResponse = networkService.sendTurn(row, column);
            } catch (IOException e) {
                gameView.showMessage("Network error");
            }
            if (sendTurnResponse != 0 && sendTurnResponse != ResponseMessageIDs.INVALID_MESSAGE) {
                field.makeStep(row, column);
                gameView.setFieldViewClickListener(null);
//            if (field.isWinner(row, column, field.getCurrentPlayerChar())) {
//                controller.setState(new GameOverState());
//            } else {
//                field.increaseStepCounter();
//                controller.setState(new WaitState());
//            }

                if (sendTurnResponse == ResponseMessageIDs.OK_MESSAGE) {
                    field.increaseStepCounter();
                    controller.setState(new WaitState(controller, gameView, field, networkService));
                } else {
                    controller.setState(new GameOverState());
                }
            } else if (sendTurnResponse == ResponseMessageIDs.INVALID_MESSAGE){
                gameView.showMessage("Invalid move");
            }
        }
    }


    @Override
    public void run() {

    }
}

//todo: 9.11.2021 homeTask
/*
 1. реализовать WaitState:
 1.1 дождаться ответа от сервера
 1.2. отобразить ответ сервера в FieldView
 1.3 изменить состояние на PlayState
 2. после хода игрока в ПлэйСтейт - отправить ход на сервер
 */