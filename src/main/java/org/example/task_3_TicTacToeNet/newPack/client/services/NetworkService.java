package org.example.task_3_TicTacToeNet.newPack.client.services;

import org.example.task_3_TicTacToeNet.newPack.common.RequestMessageIDs;
import org.example.task_3_TicTacToeNet.newPack.common.ResponseMessageIDs;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

public class NetworkService {
    private final Socket socket;
    private final BufferedOutputStream out;
    private final BufferedInputStream in;

    public NetworkService() throws IOException {
        socket = new Socket("127.0.0.1", 1337);
        out = new BufferedOutputStream(socket.getOutputStream());
        in = new BufferedInputStream(socket.getInputStream());
    }

    public void createNewGame() throws IOException {
        byte data;
        do { //подключение
            Sockets.sendSingleByte(out, RequestMessageIDs.NEW_GAME_MESSAGE);
            data = Sockets.receiveSingleByte(in);
        } while (data != ResponseMessageIDs.OK_MESSAGE);
    }

    public void joinExistingGame() throws IOException {
        byte data;
        do { //подключение
            Sockets.sendSingleByte(out, RequestMessageIDs.JOIN_GAME_MESSAGE);
            data = Sockets.receiveSingleByte(in);
        } while (data != ResponseMessageIDs.OK_MESSAGE);
    }

    public boolean sendGameName(String gameName) throws IOException {
        byte[] gameNameBytes = new byte[128];
        byte[] source = gameName.getBytes();
        int sourceSize = Math.min(source.length, 128);
        System.arraycopy(source, 0, gameNameBytes, 0, sourceSize);
        for (int i = sourceSize; i < 128; i++) {
            gameNameBytes[i] = ' ';
        }
        //отправить на сервер
        Sockets.sendAllBytes(out, gameNameBytes);
        byte response = Sockets.receiveSingleByte(in);
        return response == ResponseMessageIDs.OK_MESSAGE;
    }

    public byte sendTurn(int row, int column) throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        byteBuffer.putInt(row);
        byteBuffer.putInt(column);
        byte[] dataToSend = byteBuffer.array();
        Sockets.sendAllBytes(out, dataToSend);
        return Sockets.receiveSingleByte(in);
    }

    public int[] getOpponentTurn() throws IOException {
        byte[] receivedData = Sockets.readAll(in, 8);
        ByteBuffer byteBuffer = ByteBuffer.wrap(receivedData);
        int row = byteBuffer.getInt();
        int column = byteBuffer.getInt();
        return new int[]{row, column};
    }

    public byte receiveTurnResult() throws IOException {
        return Sockets.receiveSingleByte(in);
    }
}
