package org.example.task_3_TicTacToeNet.newPack.client.controllers;

public interface GameState {
    void run();
}
