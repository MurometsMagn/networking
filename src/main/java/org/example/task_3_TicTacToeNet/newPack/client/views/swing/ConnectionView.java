package org.example.task_3_TicTacToeNet.newPack.client.views.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Consumer;

public class ConnectionView extends JFrame{
    private JButton createNewGame = new JButton("Create new Game");
    private JButton connectToExistingGame = new JButton("Connect to existing Game");
    private JTextField gameNameField = new JTextField();
    private JLabel gameNameLabel = new JLabel("Input Game Name");
    private Consumer<String> createListener = null;
    private Consumer<String> connectListener = null;

    public ConnectionView() {
        super("Tic Tac Toe");
        setSize(800, 800);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        Container content = getContentPane();
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

        content.add(gameNameLabel);
        content.add(gameNameField);
        content.add(createNewGame);
        content.add(connectToExistingGame);

        createNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (createListener != null) {
                    String gameName = gameNameField.getText();
                    createListener.accept(gameName);
                }
            }
        });

        connectToExistingGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (connectListener != null) {
                    String gameName = gameNameField.getText();
                    connectListener.accept(gameName);
                }
            }
        });
    }

    public void setCreateListener(Consumer<String> createListener) {
        this.createListener = createListener;
    }

    public void setConnectListener(Consumer<String> connectListener) {
        this.connectListener = connectListener;
    }

    public static void main(String[] args) {
        try {
            //new ConnectionView().setVisible(true);
            new ConnectionView().setVisible(true);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}

//todo: 12.10.21
/*
 1.реализовать окно с двумя кнопками (доделать)
 2. показать ConnectionView из ConnectionState
 3. обработать нажатие кнопок в ConnectionState
 4. 
 */
