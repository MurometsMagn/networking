package org.example.task_3_TicTacToeNet.newPack.client.controllers;

import org.example.task_3_TicTacToeNet.newPack.client.services.NetworkService;

public class GameController {
    //поле
    //сеттер для поля
    private GameState state;
    private boolean isRunning = false;
    private NetworkService networkService;

    public GameController(NetworkService networkService) {
        this.networkService = networkService;
        state = new ConnectionState(networkService);
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public void stop() {
        isRunning = false;
    }

    public void run() {
        isRunning = true;
        while (isRunning) {
            state.run();
        }
    }
}
