package org.example.task_3_TicTacToeNet.newPack.client.views.swing;

import org.example.task_3_TicTacToeNet.newPack.common.Field;

import javax.swing.*;
import java.awt.*;
import java.util.function.BiConsumer;

public class GameView extends JFrame {
    private FieldView fieldView;
    private JLabel messageLabel;

    public GameView() {
        Container content = getContentPane();
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

        Field field = new Field();
        fieldView = new FieldView(field);
        add(fieldView);

        messageLabel = new JLabel("Current Player Char: " + field.getCurrentPlayerChar());
        add(messageLabel);
    }

    public void setFieldViewClickListener(BiConsumer<Integer, Integer> clickListener) { //клик по полю
        fieldView.setClickListener(clickListener);
    }

    public void showMessage(String message) {
        messageLabel.setText(message);
    }

    public static void main(String[] args) { //for tests
        JFrame frame = new GameView();
        frame.setTitle("GameViewTest");
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
//        Field field = new Field();
//        field.setCell(0, 0, 'X');
//        field.setCell(1, 0, 'x');
//        field.setCell(1, 1, '0');
//        FieldView fieldView = new FieldView(field);
//        frame.add(fieldView);
        frame.setVisible(true);
    }
}
