package org.example.task_3_TicTacToeNet.newPack.client;

import org.example.task_3_TicTacToeNet.newPack.common.RequestMessageIDs;
import org.example.task_3_TicTacToeNet.newPack.common.ResponseMessageIDs;
import org.example.task_3_TicTacToeNet.newPack.client.services.Sockets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        byte data;


        try {
            Socket socket = new Socket("127.0.0.1", 1337);
            try (BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());
                 BufferedInputStream in = new BufferedInputStream(socket.getInputStream())
            ) {
                do { //подключение
                    Sockets.sendSingleByte(out, RequestMessageIDs.JOIN_GAME_MESSAGE);
                    data = Sockets.receiveSingleByte(in);
                    System.out.println(data);
                } while (data != ResponseMessageIDs.OK_MESSAGE);
                System.out.println("connected");

                System.out.println("please input a name of a game");
                String gameName = scanner.nextLine();
                byte[] gameNameBytes = new byte[128];
                byte[] source = gameName.getBytes();
                int sourceSize = Math.min(source.length, 128);
                System.arraycopy(source, 0, gameNameBytes, 0, sourceSize);
                for (int i = sourceSize; i < 128; i++) {
                    gameNameBytes[i] = ' ';
                }
                //отправить на сервер
                Sockets.sendAllBytes(out, gameNameBytes);
                System.out.println("отправлен запрос на присоединение к игре " + gameName);
                System.out.println(Sockets.receiveSingleByte(in));
            }
            socket.close();
        } catch (IOException e) {
            System.err.println("I/O error: " + e.getMessage());
        }
    }
}



