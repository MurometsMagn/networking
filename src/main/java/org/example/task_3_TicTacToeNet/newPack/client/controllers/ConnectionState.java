package org.example.task_3_TicTacToeNet.newPack.client.controllers;

import org.example.task_3_TicTacToeNet.newPack.client.services.NetworkService;
import org.example.task_3_TicTacToeNet.newPack.client.views.swing.ConnectionView;

import java.io.IOException;

public class ConnectionState implements GameState{
    private final NetworkService networkService;
    private final ConnectionView connectionView = new ConnectionView();

    public ConnectionState(NetworkService networkService) {
        this.networkService = networkService;
    }

    @Override
    public void run() {
        connectionView.setCreateListener(this::createNewGame);
        connectionView.setConnectListener(this::connectToExistingGame);

        connectionView.setVisible(true);
    }

    private void connectToExistingGame(String gameName) {
        try {
            networkService.joinExistingGame();
            boolean connected = networkService.sendGameName(gameName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createNewGame(String gameName) {
        try {
            networkService.createNewGame();
            boolean connected = networkService.sendGameName(gameName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
