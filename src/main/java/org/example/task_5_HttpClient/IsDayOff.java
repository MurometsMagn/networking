package org.example.task_5_HttpClient;

import org.example.MyOwnLib;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Scanner;

/*
 Пользователь вводит день, месяц и год.
 Необходимо проверить, является ли эта дата рабочим или нерабочим днем.
 День, месяц и год запрашивать по отдельности как числа
 */

public class IsDayOff {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(new URI("https://isdayoff.ru/" + askData()))
                .build();
        HttpResponse<String> response = client.send(
                request,
                HttpResponse.BodyHandlers.ofString()
        );
        String replyText = switch (response.body()){
            case "0" -> "Рабочий день";
            case "1" -> "Нерабочий день";
            case "100" -> "Ошибка в дате";
            case "101" -> "Данные не найдены";
            default -> "Ошибка сервиса, код ошибки: " + response.statusCode();
        };
        System.out.println(replyText);
        System.out.println("response.statusCode() = " + response.statusCode());;
        System.out.println("response.body() = " + response.body());
    }

    private static String askData() {
        System.out.println("введите день месяца:");
        int day = MyOwnLib.keyboardInputInt(1, 31);
        System.out.println("Введите номер месяца:");
        int month = MyOwnLib.keyboardInputInt(1, 12);
        System.out.println("Введите год:");
        int year = MyOwnLib.keyboardInputInt(1900, 2050);

        return String.format("%04d%02d%02d", year, month, day);
    }
}
