package org.example.task_3_TicTacToeNet.newPack.common;

import org.junit.jupiter.api.Test;

import java.security.cert.X509Certificate;

import static org.junit.jupiter.api.Assertions.*;

class IsWinnerTest {

    @Test
    void isWinnerLeftDiagTest() {
        //char[][] f = {{' ', ' ', ' '}, {'X', 'X', ' '}, {'X', ' ', 'X'}};
        Field field = new Field(new char[][]{{' ', ' ', ' '}, {' ', 'X', ' '}, {' ', ' ', 'X'}});
        assertTrue(field.isWinner(0, 0, 'X'));
        field = new Field(new char[][]{{'X', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', 'X'}});
        assertTrue(field.isWinner(1, 1, 'X'));
        field = new Field(new char[][]{{'X', ' ', ' '}, {' ', 'X', ' '}, {' ', ' ', ' '}});
        assertTrue(field.isWinner(2, 2, 'X'));
    }

    @Test
    void isWinnerRightDiagTest() {
        Field field = new Field(new char[][]{{' ', ' ', 'X'}, {' ', 'X', ' '}, {' ', ' ', ' '}});
        assertTrue(field.isWinner(2, 0, 'X'));
        field = new Field(new char[][]{{' ', ' ', 'X'}, {' ', ' ', ' '}, {'X', ' ', ' '}});
        assertTrue(field.isWinner(1, 1, 'X'));
        field = new Field(new char[][]{{' ', ' ', ' '}, {' ', 'X', ' '}, {'X', ' ', ' '}});
        assertTrue(field.isWinner(0, 2, 'X'));
    }

    @Test
    void isWinnerVerticalTest() {
        Field field = new Field(new char[][]{{' ', 'Y', 'X'}, {'0', 'Y', ' '}, {'0', ' ', 'X'}});
        assertTrue(field.isWinner(1, 2, 'X'));
        assertTrue(field.isWinner(2, 1, 'Y'));
        assertTrue(field.isWinner(0, 0, '0'));
    }

    @Test
    void isWinnerHorizontTest() {
        Field field = new Field(new char[][]{{'0', '0', ' '}, {' ', 'Y', 'Y'}, {'X', ' ', 'X'}});
        assertTrue(field.isWinner(2, 1, 'X'));
        assertTrue(field.isWinner(1, 0, 'Y'));
        assertTrue(field.isWinner(0, 2, '0'));
    }
}